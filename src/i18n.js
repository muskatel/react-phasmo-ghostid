import i18n from "i18next";
import { initReactI18next } from 'react-i18next';
import LanguageDetector from "i18next-browser-languagedetector";
import XHR from "i18next-xhr-backend";

import translationEng from "./locales/en/translation.json";
import translationAfr from "./locales/af/translation.json";
import translationEs from "./locales/es/translation.json";
import translationDe from "./locales/de/translation.json";

i18n.use(XHR).use(LanguageDetector).use(initReactI18next).init({
  debug: false,
  lng: "en",
  fallbackLng: "en", // use en if detected lng is not available

  keySeparator: false, // we do not use keys in form messages.welcome

  interpolation: {
    escapeValue: false // react already safes from xss
  },

  // we init with resources
  resources: {
    en: {
      translations: translationEng
    },
    af: {
      translations: translationAfr
    },
    es: {
      translations: translationEs
    },
    de: {
      translations: translationDe
    },
  },

  // have a common namespace used around the full app
  ns: ["translations"],
  defaultNS: "translations",

  react: {
    wait: true
  }
});

export default i18n;