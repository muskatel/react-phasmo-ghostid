import './App.css';
import React from "react";
import Tooltip from '@material-ui/core/Tooltip';
import Checkbox from '@material-ui/core/Checkbox';

import Card from "@material-ui/core/Card";
import CardContent from '@material-ui/core/CardContent';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

// Sign icons
import Router from "@material-ui/icons/Router";             // emf_five
import Fingerprint from "@material-ui/icons/Fingerprint";   // fingerprints
import ExposureNeg1 from "@material-ui/icons/ExposureNeg1"; // freezing (bad)
import MenuBook from "@material-ui/icons/MenuBook";           // writing
import Radio from "@material-ui/icons/Radio";               // spirit_box
import ScatterPlot from "@material-ui/icons/ScatterPlot";   // ghost_orbs



import withStyles from "@material-ui/core/styles/withStyles";

// Ghost Signs
var emf_five = { name: "EMF Level 5",       icon: <Router /> }
var fingerprints = { name: "Fingerprints",  icon: <Fingerprint /> }
var freezing = { name: "Freezing Temp",     icon: <ExposureNeg1 /> }
var writing = { name: "Ghost Writing",      icon: <MenuBook /> }
var spirit_box = { name: "Spirit Box",      icon: <Radio />}
var ghost_orbs = { name: "Ghost Orbs",      icon: <ScatterPlot /> }

// var ghost_signs = [ emf_five, fingerprints, freezing, writing, spirit_box, ghost_orbs ]

// Ghost info
var banshee = { name: "Banshee", 
                signs : [ emf_five, fingerprints, freezing ] }
var demon = { name: "Demon", 
                signs: [ freezing, writing, spirit_box ] }
var jinn = { name: "Jinn",
             signs: [ emf_five, ghost_orbs, spirit_box ] }
var mare = { name: "Mare",
             signs: [ freezing, ghost_orbs, spirit_box ] }
var oni = { name: "Oni",
            signs: [ emf_five, writing, spirit_box ] }
var phantom = { name: "Phantom",
                signs: [ emf_five, freezing, ghost_orbs ] }
var poltergeist = { name: "Poltergeist",
                    signs: [ fingerprints, ghost_orbs, spirit_box ] }
var revenant = { name: "Revenant", 
                 signs: [ emf_five, fingerprints, writing ] }
var shade = { name: "Shade", 
              signs: [ emf_five, ghost_orbs, writing ] }
var spirit = { name: "Spirit", 
               signs: [ fingerprints, writing, spirit_box ] }
var wraith = { name: "Wraith",
               signs: [ fingerprints, freezing, spirit_box ] }
var yurei = { name: "Yurei", 
              signs: [ freezing, ghost_orbs, writing ] } 

const ghosts = [ 
  banshee,
  demon,
  jinn,
  mare,
  oni,
  phantom,
  poltergeist,
  revenant,
  shade,
  spirit,
  wraith,
  yurei
]

// Extra consts
const btnSize = 40

// Custom checkbox colors
const EmfFiveCheckbox = withStyles({
  root: {
    '&$checked' : {
      color: '#ffffff',
      backgroundColor: '#c20000',
    },
    '&$disabled' : {
      color: "#f0f0f0"
    },
  },
  checked: {},
  disabled: {},
})(React.forwardRef((props,ref)=><Checkbox color="default" ref ={ref} {...props} />));

const FingerprintsCheckbox = withStyles({
  root: {
    '&$checked' : {
      color: '#ffffff',
      backgroundColor: '#16cc74',
    },
    '&$disabled' : {
      color: "#f0f0f0"
    },
  },
  checked: {},
  disabled: {},
})(React.forwardRef((props,ref)=><Checkbox color="default" ref ={ref} {...props} />));

const FreezingCheckbox = withStyles({
  root: {
    '&$checked' : {
      color: '#ffffff',
      backgroundColor: '#2e73db',
    },
    '&$disabled' : {
      color: "#f0f0f0"
    },
  },
  checked: {},
  disabled: {},
})(React.forwardRef((props,ref)=><Checkbox color="default" ref ={ref} {...props} />));

const WritingCheckbox = withStyles({
  root: {
    '&$checked' : {
      color: '#ffffff',
      backgroundColor: '#d1ba8e',
    },
    '&$disabled' : {
      color: "#f0f0f0"
    },
  },
  checked: {},
  disabled: {},
})(React.forwardRef((props,ref)=><Checkbox color="default" ref ={ref} {...props} />));

const SpiritBoxCheckbox = withStyles({
  root: {
    '&$checked' : {
      color: '#ffffff',
      backgroundColor: '#691f91',
    },
    '&$disabled' : {
      color: "#f0f0f0"
    },
  },
  checked: {},
  disabled: {},
})(React.forwardRef((props,ref)=><Checkbox color="default" ref ={ref} {...props} />));

const GhostOrbsCheckbox = withStyles({
  root: {
    '&$checked' : {
      color: '#ffffff',
      backgroundColor: '#1f9123',
    },
    '&$disabled' : {
      color: "#f0f0f0"
    },
  },
  checked: {},
  disabled: {},
})(React.forwardRef((props,ref)=><Checkbox color="default" ref ={ref} {...props} />));

class AcceptDenyCheckBox extends React.Component {
  state = {
    selectedOptions: [], 
  }

  handleCheck (event, boxtype) {
    event.stopPropagation()
    if (event.target.type !== 'checkbox') return;
    // console.log(this.state.selectedOptions)
    var signs = this.state.selectedOptions
    if (signs.includes(boxtype)) {
      var index = signs.indexOf(boxtype)
      signs.splice(index,1)
    } else if (signs.length < 3) {
      signs.push(boxtype)
    }
    this.setState({
      selectedOptions: signs
    })    
  }  

  render () {
    let possible_ghosts = ghosts.filter(
      (ghost=>(ghost.signs.filter(sign => this.state.selectedOptions.includes(sign))
      .length === this.state.selectedOptions.length))
      )
    let possible_signs = Array.from(new Set(possible_ghosts.map(ghost=>ghost.signs).flat()))
    return (
      <div>
        <Card className={this.props.classes.root}>
          <CardContent>
            <Tooltip title={emf_five.name}>
              <EmfFiveCheckbox
                onClick={(event) => this.handleCheck(event, emf_five)}
                checked={this.state.selectedOptions.includes(emf_five)}
                icon={emf_five.icon}
                checkedIcon={emf_five.icon}
                disabled={!possible_signs.includes(emf_five)}
                style={{width:btnSize, height:btnSize}}
              />
            </Tooltip>
            <Tooltip title={fingerprints.name}>
              <FingerprintsCheckbox
                onClick={(event) => this.handleCheck(event, fingerprints)}
                checked={this.state.selectedOptions.includes(fingerprints)}
                icon={fingerprints.icon}
                checkedIcon={fingerprints.icon}
                disabled={!possible_signs.includes(fingerprints)}
                style={{width:btnSize, height:btnSize}}
              />
            </Tooltip>
            <Tooltip title={freezing.name}>
              <FreezingCheckbox
                onClick={(event) => this.handleCheck(event, freezing)}
                checked={this.state.selectedOptions.includes(freezing)}
                icon={freezing.icon}
                checkedIcon={freezing.icon}
                disabled={!possible_signs.includes(freezing)}
                style={{width:btnSize, height:btnSize}}
              />
            </Tooltip>
            <Tooltip title={writing.name}>
              <WritingCheckbox
                onClick={(event) => this.handleCheck(event, writing)}
                checked={this.state.selectedOptions.includes(writing)}
                icon={writing.icon}
                checkedIcon={writing.icon}
                disabled={!possible_signs.includes(writing)}
                style={{width:btnSize, height:btnSize}}
              />
            </Tooltip>
            <Tooltip title={spirit_box.name}>
              <SpiritBoxCheckbox
                onClick={(event) => this.handleCheck(event, spirit_box)}
                checked={this.state.selectedOptions.includes(spirit_box)}
                icon={spirit_box.icon}
                checkedIcon={spirit_box.icon}
                disabled={!possible_signs.includes(spirit_box)}
                style={{width:btnSize, height:btnSize}}
              />
            </Tooltip>
            <Tooltip title={ghost_orbs.name}>
              <GhostOrbsCheckbox
                onClick={(event) => this.handleCheck(event, ghost_orbs)}
                checked={this.state.selectedOptions.includes(ghost_orbs)}
                icon={ghost_orbs.icon}
                checkedIcon={ghost_orbs.icon}
                disabled={!possible_signs.includes(ghost_orbs)}
                style={{width:btnSize, height:btnSize}}
              />
            </Tooltip>
          </CardContent>
          { possible_ghosts.length === 1 ? 
          <CardContent>
            <Typography variant="h5" component="h2">
              You are dealing with a <b>{possible_ghosts[0].name}</b>
            </Typography>
          </CardContent> : null}
          <CardContent>
            <Table className={this.props.classes.table} aria-label="Testy">
              <TableHead>
                <TableRow>
                  <TableCell></TableCell>
                  <TableCell align="right"></TableCell>
                  <TableCell align="right"></TableCell>
                  <TableCell align="right"></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {ghosts.filter((ghost)=>(
                  this.state.selectedOptions.length === 0 || possible_ghosts.includes(ghost)
                )).map((ghost) => (
                  <TableRow key={ghost.name}>
                    <TableCell component="th" scope="row">
                      {ghost.name}
                    </TableCell>
                    <TableCell align="right">
                        {ghost.signs[0].name}
                      </TableCell>
                    <TableCell align="right">
                        {ghost.signs[1].name}
                    </TableCell>
                    <TableCell align="right">
                        {ghost.signs[2].name}
                      </TableCell>
                  </TableRow>
                ))}            
              </TableBody>
            </Table>
          </CardContent>
        </Card>
      </div>
    );
  }
}

const useStyles = makeStyles({
  root: {
    minWidth: 275,
    maxWidth: 650,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  table: {
    minWidth: 500,
    maxWidth: 650,
  },
});

function App() {
  const classes = useStyles();

  return (
    <div className="App" alignitems="center">
      <AcceptDenyCheckBox classes={classes} />
    </div>
  );
}

export default App;
