// Fonts
import "@fontsource/schoolbell"


import './App.css';
import i18n from "./i18n";
// import qrcode from "./frame.png"
// import ghostid_black from "./images/ghostid_black.png"
import ghostid_white from "./images/ghostid_white.png"

import React, { useState } from "react";
import { I18nextProvider, useTranslation } from "react-i18next";

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
// import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
// import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Divider from '@material-ui/core/Divider';
// import Checkbox from '@material-ui/core/Checkbox';
import Card from "@material-ui/core/Card";
import CardContent from '@material-ui/core/CardContent';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableContainer from '@material-ui/core/TableContainer';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
// import ButtonGroup from '@material-ui/core/ButtonGroup';
import { makeStyles } from '@material-ui/core/styles';
// import withStyles from "@material-ui/core/styles/withStyles";

// Dark mode
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline';
import { IconButton } from '@material-ui/core';

// Sundry icons
import Brightness2 from "@material-ui/icons/Brightness2";                   // Darkmode icon
import WbSunny from "@material-ui/icons/WbSunny";                           // Lightmode icon
import Autorenew from "@material-ui/icons/Autorenew";                       // Reset signs
// import CenterFocusStrongIcon from '@material-ui/icons/CenterFocusStrong';   // QR Code
import TranslateIcon from '@material-ui/icons/Translate';                   // Localization Menu
import ClearIcon from '@material-ui/icons/Clear';                           // General Removing items
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';               // Hints

// Interactivity Icons
import PersonIcon from '@material-ui/icons/Person';
import PeopleIcon from '@material-ui/icons/People';

// Bonus Objectives icons
import VisibilityIcon from '@material-ui/icons/Visibility';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
// import OpacityIcon from '@material-ui/icons/Opacity';
import WifiRoundedIcon from '@material-ui/icons/WifiRounded';
// import AcUnitRoundedIcon from '@material-ui/icons/AcUnitRounded';
import PermDeviceInformationRoundedIcon from '@material-ui/icons/PermDeviceInformationRounded';
import SmokingRoomsRoundedIcon from '@material-ui/icons/SmokingRoomsRounded';
import FlareIcon from '@material-ui/icons/Flare';
import BlurOnIcon from '@material-ui/icons/BlurOn';
import WhatshotIcon from '@material-ui/icons/Whatshot';
import DirectionsRunIcon from '@material-ui/icons/DirectionsRun';
import PanToolIcon from '@material-ui/icons/PanTool';
import Battery20Icon from '@material-ui/icons/Battery20';

// Extra toolbelt icons
import AvTimerIcon from '@material-ui/icons/AvTimer';
import BookmarksIcon from '@material-ui/icons/Bookmarks';

// Custom Components
import NameSelector from "./components/NameSelector/NameSelector.js"
import HuntTimer from "./components/HuntTimer/HuntTimer.js"
import GhostIndex from "./components/GhostIndex/GhostIndex.js"
import { ghosts, ghostsigns } from "./components/Ghosts/Ghosts.js"
import AboutDialogContent from "./components/AboutDialogContent/AboutDialogContent.js"
import LocalizationMenu from "./components/LocalizationMenu/LocalizationMenu.js"
const [ emf_five, fingerprints, freezing, writing, spirit_box, ghost_orbs, dots ] = ghostsigns




// Colors
const maincol = "#000000"
const seccol_dark = "#b8b8b8"
const seccol_light = "#4f4f4f"
const appbar_col = "#ededed"

class BonusObjectivesComponent extends React.Component {
  state = {
    selected_options: [],
    completed: [],
  }

  reset = () => {
    this.setState({
      selected_options: [],
      completed: [],
    })
  }

  handleClick = (value) => {
    let opts = this.state.selected_options
    if (opts.length <= 3 && !opts.map(x=>x.id).includes(value.id)){
      opts = opts.concat(value)
      this.setState({
        selected_options: opts,
      })
    }
  }

  handleDelete = (value) => {
    let opts2 = this.state.selected_options
    opts2.splice(opts2.map(x=>x.id).indexOf(value.id),1)
    this.setState({
      selected_options: opts2,
      completed: [],
    })
  }

  handleCompleted = (value) => {
    let comp = this.state.completed
    if (comp.includes(value)){
      comp.splice(comp.indexOf(value),1)
    } else {
      comp.push(value)
    }
    this.setState({
      completed: comp,
    })
  }

  render () {
    const t = this.props.t;
    let data = [
      { id:"Ghost Event", name: "Ghost Event", text: "Witness a Ghost Event",
        icon: <VisibilityIcon />, texticon: <VisibilityIcon color="secondary" /> },
      { id:"Ghost Photo", name: "Ghost Photo", text: "Capture a photo of the Ghost",
        icon: <AccountBoxIcon />, texticon: <AccountBoxIcon color="secondary"/> },
      { id:"EMF Reading", name: "EMF Reading", text: "Get a reading  with an EMF Reader", 
        icon: <WifiRoundedIcon />, texticon: <WifiRoundedIcon color="secondary"/>},
      { id:"Motion Sensor", name: "Motion Sensor", text: "Detect a ghost with a Motion Sensor",
        icon: <PermDeviceInformationRoundedIcon />, texticon: <PermDeviceInformationRoundedIcon color="secondary"/> },
      { id:"Smudge Stick", name: "Smudge Stick", text: "Use a Smudge Stick near the Ghost", 
        icon: <SmokingRoomsRoundedIcon />, texticon: <SmokingRoomsRoundedIcon color="secondary"/>},
      { id:"Crucifix", name: "Crucifix", text: "Prevent a hunt with a Crucifix", 
        icon: <FlareIcon />, texticon: <FlareIcon color="secondary"/> },
      { id:"Salt Evidence", name: "Salt Evidence", text: "Detect a ghost with Salt", 
       icon: <BlurOnIcon />, texticon: <BlurOnIcon color="secondary"/> },
       // NEW OBJECTIVES 3/27/2021
       { id:"Candle Evidence", name: "Candle Evidence", text: "Get the ghost to blow a candle out", 
       icon: <WhatshotIcon />, texticon: <WhatshotIcon color="secondary"/> },
       { id:"Escape a Hunt", name: "Escape a Hunt", text: "Have someone escape the Ghost during a hunt", 
       icon: <DirectionsRunIcon />, texticon: <DirectionsRunIcon color="secondary"/> },
       { id:"Repel the Ghost", name: "Repel the Ghost", text: "Repel a hunting Ghost using a Smudge Stick", 
       icon: <PanToolIcon />, texticon: <PanToolIcon color="secondary"/> },
       { id:"Average Sanity", name: "Average Sanity", text: "Get an average sanity below 25%", 
       icon: <Battery20Icon />, texticon: <Battery20Icon color="secondary"/> },
    ].sort((a,b) => a.id.localeCompare(b.id))
    return(
      <>
        <Grid container direction={'row'} justify={'flex-start'} alignItems={"center"} spacing={1}>
          { this.state.selected_options.length !== 3 ? 
            data.map(objective => (
              this.state.selected_options.map(x=>x.name).includes(objective.name) 
              ? <Grid item>
                  <Chip
                    label={t(objective.name)}
                    icon={objective.icon}
                    fullWidth
                    color="primary"
                    onClick={()=>this.handleDelete(objective)}
                  />
                </Grid>
              : <Grid item>
                  <Chip
                    label={t(objective.name)}
                    icon={objective.icon}
                    fullWidth
                    onClick={()=>this.handleClick(objective)}
                  />
                </Grid>
          ))
          : <> 
            <div style={{ width: '100%' }}>
              <List dense>
                { this.state.selected_options.map(objective=> (
                  <div>
                    <ListItem 
                      button 
                      onClick={()=>{this.handleCompleted(objective.text)}} 
                      style={{ textDecoration: this.state.completed.includes(objective.text) ? 'line-through' : 'none' }}
                      >
                      <ListItemIcon style={{minWidth:'30px'}}>{objective.texticon}</ListItemIcon>
                      <ListItemText
                        style={{ flexGrow: 1 }}
                        primaryTypographyProps = {{ 
                          color: "secondary", 
                          variant:"caption", 
                          style:{ 
                            fontFamily: "Schoolbell", 
                            fontSize: 15, 
                            // color: this.state.completed.includes(objective.text) ? "#" : "",
                          } 
                        }}
                      >
                        {t(objective.text)}
                        </ListItemText>
                      <ListItemSecondaryAction>
                        <IconButton
                          size="small"
                          onClick={()=>this.handleDelete(objective)}
                        >
                          <ClearIcon color="secondary"/>
                        </IconButton>
                      </ListItemSecondaryAction>
                    </ListItem>
                    { this.state.selected_options.slice(-1)[0] !== objective ? <Divider /> : null }
                  </div>
                ))}
              </List>
            </div>
            </>
            }
        </Grid>
      </>
  )}
}

class InteractivityComponent extends React.Component {
  state = {
    selected_option: null,
  }

  handleClick = (opt) => {
    this.setState({
      selected_option: opt
    })
  }

  reset () {
    this.setState({
      selected_option: null,
    })
  }
  
  render () {
    const t = this.props.t;
    return (
      <>
      { this.state.selected_option ? 
        <>{this.state.selected_option}</>
        :
      <Grid container direction={'row'} justify={'flex-start'} alignItems={"center"} spacing={1}>
        <Grid item>
          <Chip
            label={t("Alone")}
            icon={<PersonIcon />}
            fullWidth
            onClick={()=>{this.handleClick(
              <Chip
                label={t("Alone")}
                icon={<PersonIcon />}
                onDelete={() => {this.reset()}}
              />
            )}}
          />
        </Grid>
        <Grid item>
          <Chip
            label={t("Everyone")}
            icon={<PeopleIcon />}
            fullWidth
            onClick={()=>{this.handleClick(
              <Chip
                label={t("Everyone")}
                icon={<PeopleIcon />}
                onDelete={() => {this.reset()}}
            />
            )}}
          />
        </Grid>
      </Grid>
      }
      </>
    )
  }
}

class IncludeExcludeButton extends React.Component {
  state = {
    currentState: 0 
  }

  handleCheck (event,other){
    let newstate = (this.state.currentState + 1) % 3
    this.setState({
      currentState: newstate,
    })  
    other(newstate) 
  }

  reset () {
    this.setState({
      currentState: 0,
    })  
  }

  render () {
    return (
      <Tooltip arrow title={this.props.title}>
        <IconButton 
          onClick={(event) => this.handleCheck(event,this.props.onClick)} 
          style= {this.state.currentState===1 ? {backgroundColor: this.props.color, color:"#ffffff",width:40,height:40} : {width:40,height:40} }
          disabled= {this.props.disabled && this.state.currentState===0}
        >
          {this.state.currentState <= 1 ?
            this.props.icon : 
            <ClearIcon fontSize="large" style={{color: this.props.color}}/>
          }       
        </IconButton>
      </Tooltip>
    )}
}

class SignSelectorComponent extends React.Component {
  state = {
    selectedOptions: [], 
    excludedOptions: [],
    btnEmfFive: React.createRef(),
    btnFingerprints: React.createRef(),
    btnFreezing: React.createRef(),
    btnGhostWriting: React.createRef(),
    btnSpritBox: React.createRef(),
    btnGhostOrbs: React.createRef(),
    btnDots: React.createRef(),
  }



  handleCheck (buttonState,boxtype) {
    var signs = this.state.selectedOptions
    var excl = this.state.excludedOptions
    if (buttonState === 0) { //The button is NOT PRESSED
      if ( signs.includes(boxtype) ) {
        signs.splice(signs.indexOf(boxtype),1)
      }
      if ( excl.includes(boxtype) ) {
        excl.splice(excl.indexOf(boxtype),1)
      }

    }
    if (buttonState === 1) { // The button is YES
      if ( !signs.includes(boxtype) && signs.length < 3 ) {
        signs.push(boxtype)
      }
      if ( excl.includes(boxtype) ) {
        excl.splice(excl.indexOf(boxtype),1)
      }
    } 
    if (buttonState === 2) { // The button is NO
      if ( !excl.includes(boxtype) ) {
        excl.push(boxtype)
      }
      if ( signs.includes(boxtype) ) {
        signs.splice(signs.indexOf(boxtype),1)
      }
    } 
    // CHANGE STATE
    this.setState({
      selectedOptions: signs,
      excludedOptions: excl,
    })
  }
  
  reset_signs () {
    this.setState({
      selectedOptions: [],
      excludedOptions: [],
    })
    this.state.btnEmfFive.current.reset()
    this.state.btnFingerprints.current.reset()
    this.state.btnFreezing.current.reset()
    this.state.btnGhostWriting.current.reset()
    this.state.btnSpritBox.current.reset()
    this.state.btnGhostOrbs.current.reset()
    this.state.btnDots.current.reset()
  }

  render () {
    let impossible_ghosts = ghosts.filter(
      (ghost=>(ghost.signs.filter(sign => this.state.excludedOptions.includes(sign)).length === 0))
    ) 
    let possible_ghosts = impossible_ghosts.filter(
      (ghost=>(ghost.signs.filter(sign => this.state.selectedOptions.includes(sign)).length === this.state.selectedOptions.length))
    )
    let possible_signs = Array.from(new Set(possible_ghosts.map(ghost=>ghost.signs).flat()))
    let t = this.props.t;

    const evidence_to_button = [
      this.state.btnEmfFive,
      this.state.btnSpritBox,
      this.state.btnFingerprints,
      this.state.btnGhostOrbs,
      this.state.btnGhostWriting,
      this.state.btnFreezing,
      this.state.btnDots,  
    ]

    const evidence_order = [emf_five, spirit_box, fingerprints, ghost_orbs, writing, freezing, dots]

    return (
      <>
          <CardContent>
            <Table className={this.props.classes.table} stickyHeader size="small">
              <TableHead >
                <TableRow>
                  <TableCell colSpan={evidence_order.length}>{t("Select Evidence...")}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow key="GhostOptionsSelection" >
                  { evidence_order.map((evidence,index) => (
                      <TableCell align="center" size="medium" padding="none" style={{paddingTop:"5px", paddingBottom: "5px"}}>
                        <IncludeExcludeButton 
                          title={t(evidence.name)}
                          color={evidence.color}
                          icon={evidence.icon}
                          onClick={(event) => this.handleCheck(event,evidence)}
                          ref={evidence_to_button[index]}
                          disabled={!possible_signs.includes(evidence)}
                        />
                      </TableCell>
                  )) }
                </TableRow>
                <TableRow key="EvidenceLabels" >
                  { evidence_order.map(evidence => (
                        <TableCell align="center" size="medium" padding="none" style={{paddingLeft:"5px", paddingRight: "5px"}}>
                          <span 
                            style={{
                              maxWidth: '45px',
                              textAlign:'center',
                              wordWrap: 'break-word',
                              fontSize: 8, 

                              color: possible_signs.includes(evidence) ? evidence.color : "gray"
                            }}
                          >{t(evidence.name)}</span>
                        </TableCell>
                    )) }
                </TableRow>
              </TableBody>
            </Table>
          </CardContent>
        { possible_ghosts.length === 1 ? 
          <CardContent>
            <Card align="center">
              <CardContent>
                <Typography variant="h5">
                  {t("You are dealing with")+" "}{t(possible_ghosts[0].indefinite)} <b style={{color:"#ff3333"}}>{possible_ghosts[0].name}</b>
                </Typography>
              </CardContent>
            </Card>
            <Card align="center">
              <CardContent>
                <Table className={this.props.classes.table} size="small" stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell>{t("Traits")}</TableCell>
                    </TableRow>
                  </TableHead>
                  {possible_ghosts[0].strengths.map(strength => (
                    <TableRow key={strength.name}>
                      <TableCell>
                        <Grid container direction={'row'} alignItems={'center'}>
                          <Grid item xs={2} >
                            {strength.icon}
                          </Grid>
                          <Grid item xs={10}>
                            <Typography>{t(strength.description)}</Typography>                             
                          </Grid>
                        </Grid>
                      </TableCell>
                    </TableRow>
                  ))}
                  {possible_ghosts[0].weaknesses.map(weakness => (
                      <TableRow key={weakness.name}>
                        <TableCell>
                          <Grid container direction={'row'} alignItems={'center'}>
                            <Grid item xs={2} >
                              {weakness.icon}
                            </Grid>
                            <Grid item xs={10}>
                              <Typography>{t(weakness.description)}</Typography>                             
                            </Grid>
                          </Grid>
                        </TableCell>
                      </TableRow>
                  ))}
                </Table>
              </CardContent>
            </Card>
          </CardContent> 
        : null}
        <CardContent>
          <TableContainer component={Paper}>
            <Table className={this.props.classes.table} size="small" stickyHeader  >
              <TableHead>
                <TableRow>
                  <TableCell>{t("Ghost Type")}</TableCell>
                  <TableCell>{t("Signs")}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {ghosts.filter((ghost)=>(
                  (this.state.selectedOptions.length === 0 && this.state.excludedOptions.length === 0) || possible_ghosts.includes(ghost)
                )).map((ghost) => (
                  <>
                    <TableRow key={ghost.name}>
                      <TableCell component="th" scope="row" style={{width:120}} 
                      // onClick={()=>{
                      //   console.log(this.props.ghostpedia)
                      //   var gindex = this.props.ghostpedia.current.findGhostIndex(ghost.name);
                      //   this.props.ghostpedia.current.setTabIndex(gindex);
                      //   this.props.setShowGhostIndex(true);
                      // }}
                      >
                        <Typography 
                          // color= "secondary" 
                          style = {{ 
                            fontFamily: "Schoolbell", 
                            fontSize: 18, 
                          }} >
                          {t(ghost.name)}
                          </Typography>
                      </TableCell>
                      <TableCell align="left" >
                        <Grid container direction={'row'} alignItems={'flex-start'} style={{width:250}}>
                          { this.state.selectedOptions.length === 3 ||
                            ! this.state.selectedOptions.includes(ghost.signs[0]) 
                            ? <>
                              <Grid item xs={2} >
                                {ghost.signs[0].font_icon}
                              </Grid>
                              <Grid item xs={10}>
                                <Typography>{t(ghost.signs[0].name)}</Typography>                             
                              </Grid>
                              </> : null }
                          { this.state.selectedOptions.length === 3 ||
                            ! this.state.selectedOptions.includes(ghost.signs[1])
                            ? <>
                            <Grid item xs={2}>
                              {ghost.signs[1].font_icon}
                            </Grid>
                            <Grid item xs={10}>
                                <Typography>{t(ghost.signs[1].name)}</Typography>    
                            </Grid>
                            </> : null }
                          { this.state.selectedOptions.length === 3 ||
                            ! this.state.selectedOptions.includes(ghost.signs[2])
                            ? <>
                            <Grid item xs={2}>
                              {ghost.signs[2].font_icon}
                            </Grid>
                            <Grid item xs={10}>
                                <Typography>{t(ghost.signs[2].name)}</Typography>  
                            </Grid>    
                          </> : null }                    
                        </Grid>
                      </TableCell>
                    </TableRow>
                    { this.state.selectedOptions.length === 1 || this.state.selectedOptions.length === 2
                    ? <>
                      <TableRow key={ghost.name.concat("Hint")}>
                        <TableCell size ="small" colSpan ={2}>
                          <Grid container direction={'row'} alignItems={'center'} >
                            <Grid item xs={1} >
                              <HelpOutlineIcon style={{color:"#808080",width:13,height:13}}/>
                            </Grid>
                            <Grid item xs={11}>
                              <Typography style={{color:"#808080"}} variant="caption" display="block" gutterBottom> {t(ghost.hint)}</Typography>                             
                            </Grid>
                          </Grid>
                        </TableCell>                    
                      </TableRow>
                    </>
                    : null } 
                  </>
                ))}            
              </TableBody>
            </Table>
          </TableContainer>
        </CardContent>      
      </>
    );
  }
}

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 200,
    maxWidth: 650,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  table: {
    width:"100%",
  },
  wrapIcon: {
    verticalAlign: 'middle',
    display: 'inline-flex'
   },
   paperround: {
    rounded: false,
    padding: "10px",
   },
   grow: {
    flexGrow: 1,
   },
   appbar: {
    maxWidth: 650,
    },
    toolbarbot: {
      maxWidth: 650,
      top:'auto',
      bottom:'0',
    },
   
   toolbarIcon: {
    padding: theme.spacing(1.5, 1.5),
    margin: theme.spacing(0, 1),
   },
   dialogpaper: {
    margin: theme.spacing(1, 1),
   },
}));

function App() {
  const classes = useStyles();
  const lightTheme = createMuiTheme({
      palette: {
        type: 'light' ,
        action:{
          disabled: "#efefef"
        },
        primary: {
          main: maincol,
        },
        secondary: {
          main: seccol_light,
        },
      },
    })
  
  const darkTheme = createMuiTheme({
      palette: {
        type: 'dark',
        action:{
          disabled: "#545454",
        },
        primary: {
          main: maincol,
        },
        secondary: {
          main: seccol_dark,
        },
      }
    })

  const [theme, setTheme] = useState(false)
  const appliedTheme = theme ? lightTheme : darkTheme 
  const signselector_ref = React.createRef();
  const ghostname_ref = React.createRef();
  const interactivity_ref = React.createRef();
  const bonusobjs_ref = React.createRef();
  const [showLocalMenu, setShowLocalMenu] = useState(false)
  const [showAppInfo, setShowAppInfo] = useState(false)
  const [showHuntTimer, setShowHuntTimer] = useState(false)
  const [difficulty, setDifficulty] = useState(null)
  const [showGhostIndex, setShowGhostIndex] = useState(false)

  const { t } = useTranslation();

  const handlePageReset = () => {
    signselector_ref.current.reset_signs();
    ghostname_ref.current.reset_signs();
    interactivity_ref.current.reset();
    bonusobjs_ref.current.reset();
  }

  return (
    <div className="App"> 
      <I18nextProvider i18n={i18n}>
        <MuiThemeProvider theme={appliedTheme}>
          <CssBaseline/>
            <Dialog 
              open={showAppInfo} 
              onClose={() => setShowAppInfo(false)}
              PaperProps={{style: {width:400}}}
              >
                <AboutDialogContent classes={classes} todoClick={()=>setShowAppInfo(false)} t={t} />
            </Dialog>
            <Dialog 
              open={showLocalMenu} 
              onClose={() => setShowLocalMenu(false)}
              PaperProps={{style: {width:400}}}
              >
              <div>
                <LocalizationMenu 
                  lasses={classes} 
                  onClickLang={(desiredLang)=>i18n.changeLanguage(desiredLang)}
                  onClickClose={() => setShowLocalMenu(false)}
                  t={t} />
              </div>
            </Dialog>
            <AppBar position="sticky" className={classes.appbar}>
              <Toolbar>
                {/* <Typography className={classes.grow} align="left">Ghost ID</Typography> */}
                <div className={classes.grow} align="left">
                  <img src={ghostid_white} alt="ghostidtextlogo" width='100'/>
                </div>
                
                <Tooltip title={t("Languages")} className={classes.toolbarIcon} arrow>
                    <IconButton 
                      onClick={() => setShowLocalMenu(true)}
                    >
                    <TranslateIcon style={{color:appbar_col}}/> 
                  </IconButton>
                </Tooltip>
                <Tooltip title={appliedTheme === darkTheme ? t("Light Mode") : t("Dark Mode") } className={classes.toolbarIcon} arrow>
                  <IconButton 
                      onClick={() => setTheme(!theme)}
                    >
                    {appliedTheme === darkTheme 
                    ? <WbSunny style={{color:appbar_col}} /> 
                    : <Brightness2 style={{color:appbar_col}} /> }
                  </IconButton>
                </Tooltip>
                <Tooltip title={t("About GhostID")} className={classes.toolbarIcon} arrow>
                    <IconButton 
                      onClick={() => setShowAppInfo(true)}
                    >
                    <HelpOutlineIcon style={{color:appbar_col}}/> 
                  </IconButton>
                </Tooltip>
                <Tooltip title={t("Reset Page")} className={classes.toolbarIcon} arrow>
                  <IconButton 
                      onClick={() => handlePageReset()}
                    > 
                      <Autorenew style={{color:appbar_col}}/> 
                  </IconButton>
                </Tooltip>
              </Toolbar>            
            </AppBar>
          <Card className={classes.root} align="center">
            <CardContent>
              <Table className={classes.table} stickyHeader size="small">
                <TableHead >
                  <TableRow>
                    <TableCell colSpan={2}>{t("Mission Details...")}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell style={{width:125}}>{t("Ghost Name")}</TableCell>
                    <TableCell><NameSelector classes={classes} ref={ghostname_ref} t={t}/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell style={{width:125}}>{t("Interactivity")}</TableCell>
                    <TableCell><InteractivityComponent classes={classes} ref={interactivity_ref} t={t}/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell style={{width:125}} colSpan={2}>{t("Bonus Objectives")}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell colSpan={2}><BonusObjectivesComponent classes={classes} ref={bonusobjs_ref} t={t}/></TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </CardContent>

            <SignSelectorComponent classes={classes} ref={signselector_ref} t={t}/>
            
            <CardContent>
              <div>
                <Typography 
                  color= "secondary" 
                  style = {{ 
                    fontFamily: "Schoolbell", 
                    fontSize: 14, 
                  }} >
                  {t("Favicon made by")} <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> {t("from")} <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                  </Typography>
              </div>
              <div>
              <Typography                 
                  color= "secondary" 
                  style = {{ 
                    fontFamily: "Schoolbell", 
                    fontSize: 14, 
                  }} >
                  {t("Ghost ID is developed & maintained by")} <a href="https://www.reddit.com/user/thelunararmy" title="thelunararmy">u/thelunararmy</a>.
                </Typography>
              </div>
              <div>
              <Typography                 
                  color= "secondary" 
                  style = {{ 
                    fontFamily: "Schoolbell", 
                    fontSize: 14, 
                  }} >
                  {t("(Langauge) translation by (username)")}.
                </Typography>
              </div>
            </CardContent>
          </Card>


          <AppBar position="sticky" className={classes.toolbarbot}>
              <Dialog 
                open={showHuntTimer} 
                onClose={() => setShowHuntTimer(false)}
                PaperProps={{style: {width:400}}}
                >
                  <HuntTimer 
                    classes={classes} 
                    difficulty={difficulty} 
                    sDifficulty={(d)=>setDifficulty(d)} 
                    todoClick={()=>setShowHuntTimer(false)} 
                    t = {t}
                  />
              </Dialog>
              <Dialog 
                open={showGhostIndex} 
                onClose={() => setShowGhostIndex(false)}
                PaperProps={{style: {width:400}}}
                >
                  <GhostIndex 
                    classes={classes} 
                    todoClick={()=>setShowGhostIndex(false)}
                    t = {t} 
                    // ref={ghostpedia_ref}
                  />
              </Dialog>

              <Toolbar variant="dense">  
                <Tooltip title={t("Hunt Timer")} placement="top" arrow>
                  <Button 
                    fullWidth 
                    startIcon={<AvTimerIcon style={{color:appbar_col}}/>} 
                    onClick={()=>{setShowHuntTimer(true)}}
                  />
                </Tooltip>    
                <Tooltip title={t("Ghostpedia")} placement="top" arrow>
                  <Button 
                    fullWidth 
                    startIcon={<BookmarksIcon style={{color:appbar_col}}/>} 
                    onClick={()=>{setShowGhostIndex(true)}}
                  />
                </Tooltip>  
                <Button 
                  fullWidth 
                  startIcon={"?"} 
                  disabled
                />
              </Toolbar>            
            </AppBar>
        </MuiThemeProvider>
      </I18nextProvider>
    </div>
  );
}

export default App;
