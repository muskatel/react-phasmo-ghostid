// Imports
import React from "react";
import { male, female, surnames} from '../../json/names.json';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import Predictionary from "predictionary";
import TextField from '@material-ui/core/TextField';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

//Icons
// import TranslateIcon from '@material-ui/icons/Translate';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';

// Name predcition
const possible_names = male.concat(female)
const possible_surnames = surnames
const predictor_name = new Predictionary();
const predictor_surname = new Predictionary();
predictor_name.addWords(possible_names)
predictor_surname.addWords(possible_surnames)



class NameSelector extends React.Component {
    state = {
      raw_name: "",
      chip_names: [],
      chosen_name: null,
      pop_name_el: null,
      pop_name_show: false,
      raw_surname: "",
      chip_surnames: [],
      chosen_surname: null,
      pop_surname_el: null,
      pop_surname_show: false,
      name_field_ref: React.createRef(),
      surname_field_ref: React.createRef(),
    }
  
    handleChangeName = (event) => {
      var raw_input = event.target.value.toLowerCase()
      this.setState({
        raw_name: raw_input,
        pop_name_el: event.target,
      })  
      if (raw_input.length < 2){
        this.setState({
          chip_names: [],
          pop_name_show: false,
        })  
        return
      }
      var predicted_names = predictor_name.predict(raw_input)
      this.setState({
        chip_names: predicted_names,
        pop_name_show: true,
      })  
    }
  
    chooseName = (name) => {
      this.setState({
        chosen_name: name,
        chip_names: [],
        pop_name_show: false,
      })
      if (!this.state.chosen_surname){
        this.state.surname_field_ref.current.focus();
      }
    }
  
    handleChangeSurname = (event) => {
      var raw_input2 = event.target.value.toLowerCase()
      this.setState({
        raw_surname: raw_input2,
        pop_surname_el: event.target,
      })  
      if (raw_input2.length < 2){
        this.setState({
          chip_surnames: [],
          pop_surname_show: false,
        })  
        return
      }
      var predicted_surnames = predictor_surname.predict(raw_input2)
      this.setState({
        chip_surnames: predicted_surnames,
        pop_surname_show: true,
      })  
    }
  
    chooseSurname = (name) => {
      this.setState({
        chosen_surname: name,
        chip_surnames: [],
        pop_surname_show: false,
      })
      if (!this.state.chosen_name){
        this.state.name_field_ref.current.focus();
      }
    }
  
    handleReset = () => {
      this.setState({
        raw_name: "",
        chip_names: [],
        chosen_name: null,
        pop_name_el: null,
        pop_name_show: false,
        raw_surname: "",
        chip_surnames: [],
        chosen_surname: null,
        pop_surname_el: null,
        pop_surname_show: false,
      })
    }
  
    handleNameReset = () => {
      this.setState({
        raw_name: "",
        chip_names: [],
        chosen_name: null,
        pop_name_el: null,
        pop_name_show: false,
      })
    }
  
    handleSurnameReset = () => {
      this.setState({
        raw_surname: "",
        chip_surnames: [],
        chosen_surname: null,
        pop_surname_el: null,
        pop_surname_show: false,
      })
    }
  
  
    reset_signs () {
      this.handleReset();
    }
    
    render () {
      const t = this.props.t;
      return (
        <>
            { this.state.chosen_name && this.state.chosen_surname ? 
            <Grid container direction={'row'} justify={'flex-start'} alignItems={"center"} spacing={1}>
              <Chip
                icon={<PermContactCalendarIcon />}
                label={this.state.chosen_name + " " + this.state.chosen_surname}
                onDelete={() => {this.handleReset()}}
              />
            </Grid> 
            :
            <Grid container direction={'row'} justify={'flex-start'} alignItems={"center"} spacing={1}>
              { this.state.chosen_name ?  
                <Grid item xs={6}>
                  <Chip
                    label={this.state.chosen_name}
                    onDelete={() => {this.handleNameReset()}}
                  />
                </Grid> :
              <>
                <Grid item xs={6}>
                    <TextField 
                      label={t("Name")} 
                      size="small"
                      onChange={this.handleChangeName}
                      margin="dense"
                      variant="outlined"
                      fullWidth
                      value={this.state.raw_name}
                      inputRef={this.state.name_field_ref}
                    />
                  </Grid>
                  <Popper
                      open={this.state.chip_names.length}
                      anchorEl={this.state.pop_name_el}
                    >
                      <Paper style={{ 
                          minWidth: this.state.name_field_ref.current ? this.state.name_field_ref.current.offsetWidth : 0,
                          // backgroundColor: "rgba(200,100,0,.5)", // Tint color
                          filter: "invert(15%)"               
                        }}>
                        <List>
                        { this.state.chip_names.map((data) => (
                            <>
                                <ListItem button onClick= {() => {this.chooseName(data)}}>
                                  <ListItemText>{data}</ListItemText>
                                </ListItem>
                                { this.state.chip_names.slice(-1)[0] !== data && (
                                  <Divider />
                                )}
                            </>
                          ))}
                        </List>
                      </Paper>
                    </Popper>
              </>
              }
              { this.state.chosen_surname ?  
              <Grid item xs={6}>
                  <Chip
                    label={this.state.chosen_surname}
                    onDelete={() => {this.handleSurnameReset()}}
                  />
              </Grid> 
              :
              <>
                <Grid item xs={6}>
                  <TextField 
                    label={t("Surname")}
                    size="small"
                    onChange={this.handleChangeSurname}
                    margin="dense"
                    variant="outlined"
                    fullWidth
                    value={this.state.raw_surname}
                    inputRef={this.state.surname_field_ref}
                  />
                </Grid>
                <Popper
                      open={this.state.chip_surnames.length}
                      anchorEl={this.state.pop_surname_el}
                    >
                      <Paper
                        style={{ 
                            minWidth: this.state.surname_field_ref.current ? this.state.surname_field_ref.current.offsetWidth : 0,
                            filter: "invert(15%)",
                          }}>
                        <List>
                            { this.state.chip_surnames.map((data) => (
                              <>
                                <ListItem button onClick= {() => {this.chooseSurname(data)}}>
                                  <ListItemText>{data}</ListItemText>
                                </ListItem>
                                { this.state.chip_surnames.slice(-1)[0] !== data && (
                                  <Divider />
                                )}
                              </>
                            ))}
                        </List>
                      </Paper>
                    </Popper>
              </>
              }
            </Grid>
            }
        </>
      )}
  }

  export default NameSelector;