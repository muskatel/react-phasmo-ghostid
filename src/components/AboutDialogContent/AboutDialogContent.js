// Imports
import React from "react";

// Components
import Divider from '@material-ui/core/Divider';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import { IconButton } from '@material-ui/core';

// About Icons
import ClearIcon from '@material-ui/icons/Clear';  
import FeedbackIcon from '@material-ui/icons/Feedback';
// import PeopleIcon from '@material-ui/icons/People';
import PaymentIcon from '@material-ui/icons/Payment';
import CodeIcon from '@material-ui/icons/Code';
import RedditIcon from '@material-ui/icons/Reddit';
import GitHubIcon from '@material-ui/icons/GitHub';
import LocalCafeIcon from '@material-ui/icons/LocalCafe';
// import StarIcon from '@material-ui/icons/Star';
import SportsEsportsIcon from '@material-ui/icons/SportsEsports';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';


class AboutDialogContent extends React.Component {
  state = {
    tabindex: 0,
   }

   setTabIndex = (value) => {
     this.setState({
       tabindex : value,
     })
   }

  render () {
    const redditors = ["scenic_subterfuge","AverageHusband","FukkenHanzel"]
    const t = this.props.t;
      return (
        <div>
          <Toolbar>
            <Box display="flex" flexDirection="row" style={{width:'100%'}}>
                <Box justifyContent="left" alignSelf="center" p={1} flexGrow={1}>
                  <Typography 
                  variant="h6" 
                  className={this.props.classes.grow} 
                  align="left" 
                  style = {{ 
                    fontFamily: "Schoolbell", 
                  }} >
                    {t("About Ghost ID")}
                  </Typography>
                </Box>
                <Box justifyContent="right" alignSelf="center" p={1}>
                  <IconButton 
                    onClick={() => this.props.todoClick(false)}
                    size='small'
                    >
                    <ClearIcon /> 
                  </IconButton>
                </Box>
              </Box>
          </Toolbar>
          <Tabs variant="scrollable" value={this.state.tabindex} onChange={(e,v)=>{this.setTabIndex(v)}} >
            <Tab label={t("App")} style={{minWidth: 20, flexGrow: 1}} />
            <Tab label={t("Author")} style={{minWidth: 20, flexGrow: 1}} />
            <Tab label={t("Contributors")} style={{minWidth: 20, flexGrow: 1}} />
          </Tabs>
            <div hidden={this.state.tabindex !== 0}>
              <List dense>
                <ListItem>
                  <Typography variant="caption" display="block" gutterBottom>
                    {t("This is a React web app to track ghostly activity in the popular Steam game Phasmophobia. Simply tap on the evidence icons and the app will help narrow down the type of ghost haunting you. This app will work on any Firefox or Chrome browser; mobile or desktop.")}
                  </Typography>
                </ListItem>
                <Divider />
                <ListItem button component={Link} href="https://forms.gle/mfBPHcMb4uwXibPDA" color="textPrimary">
                  <ListItemIcon><FeedbackIcon /></ListItemIcon>
                  <ListItemText>{t("Give Feedback")}</ListItemText>
                </ListItem>
                <ListItem button component={Link} href="https://store.steampowered.com/app/739630/Phasmophobia/" color="textPrimary">
                  <ListItemIcon><PaymentIcon /></ListItemIcon>
                  <ListItemText>{t("Buy Phasmophobia")}</ListItemText>
                </ListItem>
                <ListItem button component={Link} href="https://gitlab.com/thelunararmy/react-phasmo-ghostid" color="textPrimary">
                  <ListItemIcon><CodeIcon /></ListItemIcon>
                  <ListItemText>{t("Source Code")}</ListItemText>
                </ListItem>
                <ListItem button component={Link} href="https://steamcommunity.com/sharedfiles/filedetails/?id=2344621691" color="textPrimary">
                  <ListItemIcon><SportsEsportsIcon /></ListItemIcon>
                  <ListItemText>{t("Ghost ID Steam Page")}</ListItemText>
                </ListItem>
                <Divider />
                <Tooltip title={t("Donations are not needed for continued development. But are greatly appreciated!")}>
                  <ListItem button component={Link} href="https://ko-fi.com/thelunararmy" color="textPrimary">
                    <ListItemIcon><LocalCafeIcon /></ListItemIcon>
                    <ListItemText>{t("Buy me a Coffee")}</ListItemText>
                  </ListItem>
                </Tooltip>
              </List>
            </div>
            <div hidden={this.state.tabindex !== 1}>
              <List dense>
                <ListItem>
                  <Typography variant="caption" display="block" gutterBottom>
                    Stuff about me. I am a programmer in South-Africa. I created Ghost ID to reteach myself React and Material UI in preperation to learn Bootstrap. I like playing Phasmophobia with my friends, but we all dislike the in-game journal. So I combined the two needs and here we are. My personal contact info can be seen below...
                  </Typography>
                </ListItem>
                <Divider />
                <ListItem button component={Link} href="https://www.reddit.com/user/thelunararmy" color="textPrimary">
                  <ListItemIcon><RedditIcon /></ListItemIcon>
                  <ListItemText>Reddit</ListItemText>
                </ListItem>
                <ListItem button component={Link} href="https://gitlab.com/thelunararmy" color="textPrimary">
                  <ListItemIcon><GitHubIcon /></ListItemIcon>
                  <ListItemText>GitLab</ListItemText>
                </ListItem>
                <ListItem button component={Link} href="https://steamcommunity.com/id/LegendaryZA/" color="textPrimary">
                  <ListItemIcon><SportsEsportsIcon /></ListItemIcon>
                  <ListItemText>Steam</ListItemText>
                </ListItem>
                <ListItem button component={Link} href="https://ko-fi.com/thelunararmy" color="textPrimary">
                  <ListItemIcon><LocalCafeIcon /></ListItemIcon>
                  <ListItemText>Kofi</ListItemText>
                </ListItem>
              </List>
            </div>
            <div hidden={this.state.tabindex !== 2}>
              <List dense>
                <ListItem>
                  <Typography variant="caption" display="block" gutterBottom>
                    {t("Thank you to everyone who contributed to Ghost ID, either through feedback, testing, or code assistance.")}
                  </Typography>
                </ListItem>
                <Divider />
                <ListItem button component={Link} href="https://gitlab.com/EternalDeiwos" color="textPrimary">
                  <ListItemIcon><FavoriteBorderIcon /></ListItemIcon>
                  <ListItemText primary="EternalDeiwos" secondary="Helped debug the GitLab pipeline" />
                </ListItem>
                <ListItem button component={Link} href="https://gitlab.com/muskatel" color="textPrimary">
                  <ListItemIcon><FavoriteBorderIcon /></ListItemIcon>
                  <ListItemText primary="MuskateL" secondary="Feedback and feature suggestions" />
                </ListItem>
                <ListItem button component={Link} href="https://www.reddit.com/user/b_ootay_ful/" color="textPrimary">
                  <ListItemIcon><FavoriteBorderIcon /></ListItemIcon>
                  <ListItemText primary="JackGrimm" secondary="Inventor of the web based journal" />
                </ListItem>
                <Divider />
                <ListItem>
                  <Typography variant="caption" display="block" gutterBottom>
                    {t("User Feedback")}
                  </Typography>
                </ListItem>
                {redditors.map(user=>(
                  <ListItem button component={Link} href={`https://www.reddit.com/user/${user}`} color="textPrimary">
                    <ListItemIcon><RedditIcon /></ListItemIcon>
                    <ListItemText primary={user} />
                  </ListItem>
                ))}
                <ListItem button component={Link} href={`https://gitlab.com/Svarr`} color="textPrimary">
                    <ListItemIcon><GitHubIcon /></ListItemIcon>
                    <ListItemText primary={"Svarr"} />
                  </ListItem>
              </List>
            </div>
        </div>
      )}
}
export default AboutDialogContent;