import React from 'react';

//placeholder until a better icon is chosen
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';

// Traits icons
import MyLocation from '@material-ui/icons/MyLocation';   // Banshee
import CampaignIcon from '@mui/icons-material/Campaign'; // Banshee
import TrendingDownIcon from '@mui/icons-material/TrendingDown'; // Banshee
import WarningRoundedIcon from '@material-ui/icons/WarningRounded'; // Demon
import FlareIcon from '@material-ui/icons/Flare';         // Demon
// import LoyaltyIcon from '@material-ui/icons/Loyalty';     // Demon
import VisibilityTwoToneIcon from '@mui/icons-material/VisibilityTwoTone'; // Daegon
import ElectricBoltIcon from '@mui/icons-material/ElectricBolt'; // Daegon
import SpeedIcon from '@material-ui/icons/Speed';         // Jinn
import PowerOffIcon from '@material-ui/icons/PowerOff';   // Jinn
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle'; // Mare
import WbIncandescentIcon from '@material-ui/icons/WbIncandescent';             // Mare
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount'; // Oni
import FitnessCenterIcon from '@material-ui/icons/FitnessCenter';         // Oni
import TrendingUpIcon from '@material-ui/icons/TrendingUp';               // Oni
import VisibilityOff from '@material-ui/icons/VisibilityOff';   // Phantom
import CameraAltIcon from '@material-ui/icons/CameraAlt';       // Phantom
import SportsHandballIcon from '@material-ui/icons/SportsHandball';         // Poltergeist
import RemoveShoppingCartIcon from '@material-ui/icons/RemoveShoppingCart'; // Poltergeist
import DirectionsRunIcon from '@material-ui/icons/DirectionsRun'; // Revenant
import FindReplaceIcon from '@material-ui/icons/FindReplace';     // Revenant
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';     // Revenent
import SearchIcon from '@material-ui/icons/Search'; // Shade
import GroupIcon from '@material-ui/icons/Group';   // Shade
import ChildCareIcon from '@material-ui/icons/ChildCare'; // Spirit
import SmokeFreeIcon from '@material-ui/icons/SmokeFree'; // Spirit
import VolumeOffIcon from '@material-ui/icons/VolumeOff';         // Wraith
import NoMeetingRoomIcon from '@material-ui/icons/NoMeetingRoom'; // Wraith
import BlurOnIcon from '@material-ui/icons/BlurOn';               // Wraith
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied'; // Yuri
import SmokingRoomsIcon from '@material-ui/icons/SmokingRooms';                           // Yuri
import WhatshotIcon from '@material-ui/icons/Whatshot'; // Hantu
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver'; // Yokai
import VolumeDownIcon from '@material-ui/icons/VolumeDown'; // Yokai
import VideocamOffIcon from '@material-ui/icons/VideocamOff'; // Goryo
import HomeIcon from '@material-ui/icons/Home'; // Goryo
// import VolumeOffIcon from '@material-ui/icons/VolumeOff'; // Myling
import HearingIcon from '@material-ui/icons/Hearing'; // Myling
import PanToolIcon from '@mui/icons-material/PanTool'; // Obake
import PersonAddIcon from '@mui/icons-material/PersonAdd'; // Obake
import FireplaceIcon from '@mui/icons-material/Fireplace'; // Onryo
import FireExtinguisherIcon from '@mui/icons-material/FireExtinguisher'; // Onryo
import ElectricBikeIcon from '@mui/icons-material/ElectricBike'; // Raiju
import ElectricalServicesIcon from '@mui/icons-material/ElectricalServices'; // Raiju
import PeopleOutlineIcon from '@mui/icons-material/PeopleOutline'; // The Twins
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow'; // The Twins
import CopyAllIcon from '@mui/icons-material/CopyAll'; // The Mimic
import FilterTiltShiftIcon from '@mui/icons-material/FilterTiltShift'; // The Mimic

// import HelpOutlineIcon from '@material-ui/icons/HelpOutline'; // Placeholder

// Sign icons
import Router from "@material-ui/icons/Router";             // emf_five
import Fingerprint from "@material-ui/icons/Fingerprint";   // fingerprints
import AcUnit from "@material-ui/icons/AcUnit";             // freezing (bad)
import MenuBook from "@material-ui/icons/MenuBook";         // writing
import Radio from "@material-ui/icons/Radio";               // spirit_box
import ScatterPlot from "@material-ui/icons/ScatterPlot";   // ghost_orbs
import GradientIcon from '@material-ui/icons/Gradient';     // dots

// Colors
const good = "#33ff33"
const bad = "#ff3333"

// Ghost Signs
let emf_five = { name: "EMF Level 5",       icon: <Router  />,      font_icon: <Router fontSize="small" />,      color: '#c20000'}
let fingerprints = { name: "Fingerprints",  icon: <Fingerprint />,  font_icon: <Fingerprint fontSize="small" />, color: '#16cc74'}
let freezing = { name: "Freezing Temp",     icon: <AcUnit />,       font_icon: <AcUnit fontSize="small" />,      color: '#2e73db'}
let writing = { name: "Ghost Writing",      icon: <MenuBook />,     font_icon: <MenuBook fontSize="small" />,    color: '#916816'}
let spirit_box = { name: "Spirit Box",      icon: <Radio />,        font_icon: <Radio fontSize="small" />,       color: '#b536ff'}
let ghost_orbs = { name: "Ghost Orbs",      icon: <ScatterPlot />,  font_icon: <ScatterPlot fontSize="small" />, color: '#7ec4b0'}
let dots =       { name: "DOTS Projector ", icon: <GradientIcon />, font_icon: <GradientIcon fontSize="small" />,color: '#1f9123'}


// Ghost info
let banshee = { 
                name: "Banshee", 
                signs : [ fingerprints, ghost_orbs, dots ],
                hint: "Only interacts with one specific player...",
                indefinite: "a ",
                strengths : [
                  {
                  name: "Focussed Hunter",
                  description: "Will focus on one player at a time until it kills them.",
                  icon: <MyLocation style={{ color: bad }} />,
                },
                {
                  name: "Hunter's Mark",
                  description: "The Banshee will weaken their target before striking.",
                  icon: <TrendingDownIcon style={{ color: bad }} />,
                }
              ],
                weaknesses : [
                //   {
                //   name: "Fears Crucifix",
                //   description: "Crucifix radius increased from 3 to 5 meters.",
                //   icon: <FlareIcon style={{ color: good }}/>,
                // }
                {
                  name: "A Screamer",
                  description: "This ghost can be heard screaming when using a parabolic microphone.",
                  icon: <CampaignIcon style={{ color: good }}/>,
                }
              ],
              }
let demon = { name: "Demon", 
                signs: [ fingerprints, writing, freezing ],
                hint: "Hunts very often without reason...",
                indefinite: "a ",
                strengths : [{
                  name: "Aggressive",
                  description: "Will enter hunt mode very frequently without provocation. Beware!",
                  icon: <WarningRoundedIcon style={{ color: bad }} />,
                }],     
                weaknesses : [
                  {
                    name: "Fears Crucifix",
                    description: "Crucifix radius increased from 3 to 5 meters.",
                    icon: <FlareIcon style={{ color: good }}/>,
                  }
                //   {
                //   name: "Ouija Talker",
                //   description: "Asking a Demon successful questions on the Ouija Board will not lower Sanity.",
                //   icon: <LoyaltyIcon  style={{ color: good }} />,
                // }
              ],    
              }
let jinn = { name: "Jinn",
             signs: [ emf_five, fingerprints, freezing ], 
             hint: "Moves very quickly while the power is on...",
             indefinite: "a ",
             strengths : [{
              name: "Fleet footed",
              description: "Will travel at very fast speeds if its victim is far away.",
              icon: <SpeedIcon style={{ color: bad }} />,
            }], 
             weaknesses : [{
              name: "Requires Pylons",
              description: "Turning off the building's power will prevent the Jinn from moving fast.",
              icon: <PowerOffIcon style={{ color: good }} />,
            }],
           }
let mare = { name: "Mare",
             signs: [ spirit_box, ghost_orbs, writing ],
             hint: "Will try to turn light sources off and trip the breaker...",
             indefinite: "a ",
             strengths : [{
              name: "Shadow Powered",
              description: "Hunts more often in the dark. Will attempt to turn off lights or tripping the power.",
              icon: <SupervisedUserCircleIcon style={{ color: bad }} />,
            }], 
             weaknesses : [{
              name: "Heliophobic",
              description: "Lowered chance of hunts in well lit or bright areas.",
              icon: <WbIncandescentIcon style={{ color: good }} />,
            }],
           }
let oni = { name: "Oni",
            signs: [ emf_five, freezing, dots ],
            hint: "More active when multiple players are nearby...",
            indefinite: "an ",
            strengths : [{
              name: "Territorial",
              description: "More active when multiple people are inside its domain.",
              icon: <SupervisorAccountIcon style={{ color: bad }} />,
            },
            {
              name: "Strongarm",
              description: "Will throw objects at great speeds over long distances.",
              icon: <FitnessCenterIcon style={{ color: bad }} />,
            }], 
             weaknesses : [{
              name: "Hyperactive",
              description: "Increased player activity will make the Oni easier to find and identify.",
              icon: <TrendingUpIcon style={{ color: good }} />,
            }],
           }
let phantom = { name: "Phantom",
                signs: [ spirit_box, fingerprints, dots ],
                hint: "Looking at the ghost will drop your sanity a lot...",
                indefinite: "a ",
                strengths : [{
                  name: "Mean Look",
                  description: "Looking at a Phantom will considerably drop your Sanity.",
                  icon: <VisibilityOff style={{ color: bad }} />,
                }], 
                 weaknesses : [{
                  name: "Camera Shy",
                  description: "Taking a photo of a Phantom will make it disappear for a while.",
                  icon: <CameraAltIcon style={{ color: good }} />,
                }],
               }
let poltergeist = { name: "Poltergeist",
                    signs: [ spirit_box, fingerprints, writing ],
                    hint: "Moves and throws objects around often...",
                    indefinite: "a ",
                    strengths : [{
                      name: "Multi-Armed",
                      description: "Can throw several objects and close multiple doors at the same time.",
                      icon: <SportsHandballIcon style={{ color: bad }}/>,
                    }], 
                     weaknesses : [{
                      name: "Kleptomaniac",
                      description: "Requires throwable items within its domain to be effective.",
                      icon: <RemoveShoppingCartIcon style={{ color: good }} />, 
                    }],
                   }
let revenant = { name: "Revenant", 
                 signs: [ ghost_orbs, writing, freezing ],
                 hint: "Hiding makes the ghost move slowly during hunts...",
                 indefinite: "a ",
                 strengths : [{
                  name: "Marathon Pro",
                  description: "A Revenant runs very quickly while hunting. You cannot outrun it.",
                  icon: <DirectionsRunIcon style={{ color: bad }} />,
                },
                {
                  name: "Adaptive Hunter",
                  description: "Can freely switch victims during hunts, preferring whomever is closer.",
                  icon:<FindReplaceIcon style={{ color: bad }} />,
                }], 
                 weaknesses : [{
                  name: "Slow Seeker",
                  description: "Hiding from a Revenant will make it move very slowly.",
                  icon: <MeetingRoomIcon style={{ color: good }} />,
                }],
                }

let shade = { name: "Shade", 
              signs: [ emf_five, writing, freezing ],
              hint: "Only shows itself to a lone players...",
              indefinite: "a ",
              strengths : [{
                name: "Recluse",
                description: "Will only reveal its presence to lone players. Very hard to detect in groups.",
                icon: <SearchIcon style={{ color: bad }} />,
              }], 
               weaknesses : [{
                name: "Selective Hunter",
                description: "Rarely hunts when players are grouped together.",
                icon: <GroupIcon style={{ color: good }} />,
              }],
             }

let spirit = { name: "Spirit", 
               signs: [ emf_five, spirit_box, writing ],
               hint: "A common ghost, no distinguishing features...",
               indefinite: "a ",
               strengths :  [
                {
                  name: "No Strength",
                  description: "No strengths, but easily confused with other ghost types.",
                  icon: <ChildCareIcon style={{ color: good }} />,
                },
                ], 
               weaknesses : [
              {
                name: "Asthmatic",
                description: "Using Smudge Sticks on a Spirit will stop it from attacking for 120 seconds instead of 90.",
                icon: <SmokeFreeIcon style={{ color: good }} />,
              }],
             }

let wraith = { name: "Wraith",
               signs: [ emf_five, spirit_box, dots ],
               hint: "Doesn't make audible footsteps often and hates salt...",
               indefinite: "a ",
               strengths : [{
                name: "Floating",
                description: "Wraiths rarely make footstep noise, but will leave footprints in Salt.",
                icon: <VolumeOffIcon style={{ color: bad }} />,
              },
              {
                name: "No Clip",
                description: "Can walk through walls and doors without opening them.",
                icon: <NoMeetingRoomIcon style={{ color: bad }} />,
              }], 
               weaknesses : [{
                name: "Atopic Dermatitis",
                description: "Has a toxic reaction to Salt. Will immediately stop attacking if it comes in contact with Salt.",
                icon: <BlurOnIcon style={{ color: good }} />,
              }],
               }

let yurei = { name: "Yurei", 
              signs: [ ghost_orbs, freezing, dots ],
              hint: "Passively reduces your sanity faster...",
              indefinite: "a ",
              strengths : [{
                name: "Syphon",
                description: "A Yurei will rapidly reduce your Sanity every second.",
                icon: <SentimentVeryDissatisfiedIcon style={{ color: bad }} />,

              }], 
               weaknesses : [{
                name: "Smoke Trapped",
                description: "Using a Smudge Stick inside the Yurei's domain will trap it there for about 90 seconds.",
                icon: <SmokingRoomsIcon style={{ color: good }} />,
              }],
             } 

let hantu = { name: "Hantu",
             signs: [ fingerprints, ghost_orbs, freezing ],
             hint: "Many hunts during cold temperatures...",
             indefinite: "a ",
             strengths : [{
              name: "Frost Speed",
              description: "Lower temperatures can cause the Hantu to move faster.",
              icon: <DirectionsRunIcon style={{ color: bad }} />,

            }], 
             weaknesses : [{
              name: "Coldfeet",
              description: "A Hantu will move slower in warm areas.",
              icon: <WhatshotIcon style={{ color: good }} />,
            }],
          }

  let yokai = { name: "Yokai",
             signs: [ spirit_box, ghost_orbs, dots ],
             hint: "Frequent hunts when talking...",
             indefinite: "a ",
             strengths : [{
              name: "Silence Lover",
              description: "Talking near a Yokai will anger it and increase it's chance to attack.",
              icon: <RecordVoiceOverIcon style={{ color: bad }} />,

            }], 
             weaknesses : [{
              name: "Slightly Deaf",
              description: "When hunting, a Yokai can only hear voices near it.",
              icon: <VolumeDownIcon style={{ color: good }} />,
            }],
          }

  let goryo = { 
            name: "Goryo",
            signs: [ emf_five, fingerprints, dots ],
            hint: "Can only be seen using a video camera...",
            indefinite: "a ",
            strengths : [{
              name: "Camerashy",
              description: "Will only show itself on camera if there are no people nearby.",
              icon: <VideocamOffIcon style={{ color: bad }} />,
          
            }], 
            weaknesses : [{
              name: "Homesick",
              description: "Rarely seen far from their place of death.",
              icon: <HomeIcon style={{ color: good }} />,
            }],
          }

  let myling = { 
            name: "Myling",
            signs: [ emf_five, fingerprints, writing ],
            hint: "Its footsteps can be heard closeby while hunting...",
            indefinite: "a ",
            strengths : [{
              name: "Silent Hunter",
              description: "Known to be quieter when hunting",
              icon: <VolumeOffIcon style={{ color: bad }} />,
          
            }], 
            weaknesses : [{
              name: "Loudmouth",
              description: "Frequently makes paranormal sounds while active.",
              icon: <HearingIcon style={{ color: good }} />,
            }],
          }

  let obake = { 
            name: "Obake",
            signs: [ emf_five, fingerprints, ghost_orbs ],
            hint: "Their hands leave six fingersprints...",
            indefinite: "an ",
            strengths : [{
              name: "Clean hands",
              description: "Will leave less fingerprints than normal, and fingerprints will disappear faster.",
              icon: <PanToolIcon style={{ color: bad }} />,
          
            }], 
            weaknesses : [{
              name: "Shapeshifter",
              description: "Sometimes this ghost will change its shape, and leave behind unique evidence.",
              icon: <PersonAddIcon style={{ color: good }} />,
            }],
          }

  let onryo = { 
            name: "Onryo",
            signs: [ spirit_box, ghost_orbs, freezing ],
            hint: "It hunts more frequently when there is no open flames...",
            indefinite: "an ",
            strengths : [{
              name: "Pyrophobe",
              description: "Extinguishing a flame can cause an Onryo to hunt.",
              icon: <FireplaceIcon style={{ color: bad }} />,
          
            }], 
            weaknesses : [{
              name: "Candle Blower",
              description: "Will blow out candles more often than other ghosts... but beware of a hunt afterwards.",
              icon: <FireExtinguisherIcon style={{ color: good }} />,
            }],
          }

  let raiju = { 
            name: "Raiju",
            signs: [ emf_five, ghost_orbs, dots ],
            hint: "Activity increases when we bring more electronic equipment near it...",
            indefinite: "a ",
            strengths : [{
              name: "Tech Mosquito",
              description: "The Raiju will move significantly faster as it syphons power from equipment.",
              icon: <ElectricBikeIcon style={{ color: bad }} />,
          
            }], 
            weaknesses : [{
              name: "Digital Papertrail",
              description: "It will constantly interfere with electronic equipment during hunts, making it easier to track.",
              icon: <ElectricalServicesIcon style={{ color: good }} />,
            }],
          }
          
    let the_twins = { 
            name: "The Twins",
            signs: [ emf_five, spirit_box, freezing ],
            hint: "Two paranormal activities are happening at the same time...",
            indefinite: "",
            strengths : [{
              name: "Prepare for trouble",
              description: "Either Twin can be angered to start a hunt.",
              icon: <PeopleOutlineIcon style={{ color: bad }} />,
          
            }], 
            weaknesses : [{
              name: "And make it double",
              description: "Both Twins will often interact with the environment at the same time.",
              icon: <DoubleArrowIcon style={{ color: good }} />,
            }],
          }

    let the_mimic = { 
            name: "The Mimic",
            signs: [ spirit_box, freezing, fingerprints ],
            hint: "It seems to be copying other ghost behavior...",
            indefinite: "",
            strengths : [{
              name: "Unknown for now",
              description: "Data inconsistant... we don't know what its strength is yet.",
              icon: <CopyAllIcon style={{ color: bad }} />,
          
            }], 
            weaknesses : [{
              name: "Orbs everywhere",
              description: "Ghost orbs are seen very frequently near the Mimic.",
              icon: <FilterTiltShiftIcon style={{ color: good }} />,
            }],
          }
    
let deogen = {  name: "Deogen",
            signs: [spirit_box, writing, dots],
            hint: "",
            indefinite: "a",  
            strengths : [{
              name: "Sense Living",
              description: "Deogen constantly sense the living. You can run but you can't hide.",
              icon: <VisibilityTwoToneIcon style={{ color: bad }} />,
            }],     
            weaknesses : [
              {
                name: "Slow",
                description: "Deogen require a lot of energy to form and will move very slowly when approaching its victim.",
                icon: <ElectricBoltIcon style={{ color: good }}/>,
              }
              ],
              } 

let moroi = {  name: "Moroi",
                signs: [spirit_box, writing, freezing],
                hint: "",
                indefinite: "the",  
                strengths : [{
                  name: "Strength",
                  description: "The weaker their victims, the stronger the Moroi becomes.",
                  icon: <QuestionMarkIcon style={{ color: bad }} />,
                }],     
                weaknesses : [
                  {
                    name: "Weakness",
                    description: "Moroi suffer from hyperosmia, weakening them for longer periods.",
                    icon: <QuestionMarkIcon style={{ color: good }}/>,
                  }
                  ],
                  } 

let thaye = {  name: "Thaye",
                signs: [ghost_orbs, writing, dots],
                hint: "",
                indefinite: "the",  
                strengths : [{
                  name: "Strength",
                  description: "Upon entering the location, Thaye will become active, defensive and agile",
                  icon: <QuestionMarkIcon style={{ color: bad }} />,
                }],     
                weaknesses : [
                  {
                    name: "Weakness",
                    description: "Thaye will weaken over time, making them weaker, slower and less aggressive",
                    icon: <QuestionMarkIcon style={{ color: good }}/>,
                  }
                  ],
                  } 

export const ghosts = [ // Same order as wiki page: https://phasmophobia.fandom.com/wiki/Ghosts
    banshee,
    demon,
    deogen,
    goryo,
    hantu,
    jinn,
    mare,
    moroi,
    myling,
    obake,
    oni,
    onryo,
    phantom,
    poltergeist,
    raiju,
    revenant,
    shade,
    spirit,
    thaye,
    the_mimic,
    the_twins,
    wraith,
    yokai,
    yurei,   
]

export const ghostsigns = [
    emf_five,
    fingerprints,
    freezing,
    writing,
    spirit_box,
    ghost_orbs,
    dots,
]

const toExport = { ghosts, ghostsigns };

export default toExport;