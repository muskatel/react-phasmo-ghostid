// Imports
import React from "react";
import { CountdownCircleTimer } from 'react-countdown-circle-timer'

// Components
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import { IconButton } from '@material-ui/core';

import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

// Icons
import ClearIcon from '@material-ui/icons/Clear';  
import MicOffIcon from '@material-ui/icons/MicOff';
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';
// import InputIcon from '@material-ui/icons/Input';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import LockIcon from '@material-ui/icons/Lock';


// Colors
const good = "#33ff33"
const bad = "#ff3333"
const darkred = "#ee3333"

class HuntTimer extends React.Component {
  state = {
    tabindex: this.props.difficulty,
    rng: 0,
   }

   setTabIndex = (value) => {
     this.setState({
       tabindex : value,
       rng: this.state.rng + 1,
     })
     this.props.sDifficulty(value);
   }

   renderTime = ({ remainingTime }) => {
    let t = this.props.t;
    if (remainingTime === 0) {
      return (
        <div className="timer">
          <Typography align="center" color="secondary" >{t("Hunt over.")}</Typography>
          <Typography align="center" variant="h5" className="text">{t("You're safe!")}</Typography>
          <Typography align="center" color="secondary">{t(". . . for now")}</Typography>
        </div>)
    }
  
    return (
      <div className="timer">
        <Typography align="center" color="secondary" >{t("Hunting for")}</Typography>
        <Typography align="center" variant="h3" className="value">{remainingTime}</Typography>
        <Typography align="center" color="secondary" >{t("seconds")}</Typography>
      </div>
    );
  };

  renderHints = () => {
    const hints = [
      { text: "Hide inside an empty room. Close the door.", icon: <MeetingRoomIcon style={{color:good}} />},
      { text: "Break line-of-sight with the Ghost.", icon: <VisibilityOffIcon style={{color:good}} />},
      { text: "Silence! Do not use your microphone.", icon: <MicOffIcon style={{color:bad}} />},
      { text: "The building is locked. Do not try to escape.", icon: <LockIcon style={{color:bad}} />},      
    ]
    const t = this.props.t;

    return (
      <div>
        <List dense>
          <ListItem>
            <Typography variant="caption" display="block" gutterBottom style = {{ fontFamily: "Schoolbell", size:16 }} >
              {t("Some hints to keep you breathing...")}
            </Typography>
          </ListItem>
          <Divider />
          {hints.map(hint=>(
                  <ListItem>
                    <ListItemIcon>{hint.icon}</ListItemIcon>
                    <ListItemText primary={t(hint.text)} />
                  </ListItem>
                ))}
        </List>
      </div>
    )
  }

  render () {
    let t = this.props.t;
      return (
        <div>
            <Toolbar>
            <Box display="flex" flexDirection="row" style={{width:'100%'}}>
                <Box justifyContent="left" alignSelf="center" p={1} flexGrow={1}>
                {this.state.tabindex === null  ? (
                    <div>
                        <Box display="flex" flexDirection="column" alignItems="center" style={{width:'100%'}}>
                        <Typography align="center" variant="body" className="value">{t("Select a difficulty...")}</Typography>
                        <Typography align="center" variant="caption" className="value">{t("(you will only need to do this once)")}</Typography>
                        </Box>
                    </div>
                ) : 
                <div>
                    <Typography 
                        variant="h6" 
                        className={this.props.classes.grow} 
                        align="left" 
                        style = {{ 
                        fontFamily: "Schoolbell", 
                        }} >
                        {t("Hunt Timer")}
                    </Typography>
                </div>
              }
                </Box>
                <Box justifyContent="right" alignSelf="center" p={1}>
                  <IconButton 
                    onClick={() => this.props.todoClick(false)}
                    size='small'
                    >
                    <ClearIcon /> 
                  </IconButton>
                </Box>
              </Box>
              </Toolbar>
            
            <Tabs variant="scrollable" value={this.state.tabindex} onChange={(e,v)=>{this.setTabIndex(v)}} >
                <Tab label={t("Amateur")} style={{minWidth: 20, flexGrow: 1}} />
                <Tab label={t("Intermediate")} style={{minWidth: 20, flexGrow: 1}} />
                <Tab label={t("Professional")} style={{minWidth: 20, flexGrow: 1}} />
            </Tabs>
            <div hidden={this.state.tabindex !== 0}>
                <Box display="flex" flexDirection="column" alignItems="center" style={{width:'100%', marginTop:5, marginBottom:5}}>   
                    <Box p={1}>
                        <CountdownCircleTimer
                            isPlaying
                            duration={25}
                            key={this.state.rng}
                            colors={[
                              [darkred, 0.90],
                              [darkred, 0.05],
                              ['#55ff55'],
                            ]}
                            >
                                {this.renderTime}
                        </CountdownCircleTimer>
                    </Box>
                </Box>
            </div>
            <div hidden={this.state.tabindex !== 1}>
                <Box display="flex" flexDirection="column" alignItems="center" style={{width:'100%',marginTop:5, marginBottom:5}}>   
                    <Box p={1}>
                        <CountdownCircleTimer
                            isPlaying
                            duration={35}
                            key={this.state.rng}
                            colors={[
                              [darkred, 0.90],
                              [darkred, 0.05],
                              ['#55ff55'],
                            ]}
                            >
                                {this.renderTime}
                        </CountdownCircleTimer>
                    </Box>
                </Box>     
            </div>
            <div hidden={this.state.tabindex !== 2}>
                <Box display="flex" flexDirection="column" alignItems="center" style={{width:'100%',marginTop:5, marginBottom:5}}>   
                    <Box p={1}>
                        <CountdownCircleTimer
                            isPlaying
                            duration={50}
                            key={this.state.rng}
                            colors={[
                              [darkred, 0.90],
                              [darkred, 0.05],
                              ['#55ff55'],
                            ]}
                            >
                                {this.renderTime}
                        </CountdownCircleTimer>
                    </Box>
                </Box>    
            </div>            
            <div hidden={this.state.tabindex === null}>
              {this.renderHints()} 
            </div>
    </div>
    )}
}
export default HuntTimer;