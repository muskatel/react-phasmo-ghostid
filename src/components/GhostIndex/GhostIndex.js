// Imports
import React from "react";

// Components
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import { IconButton } from '@material-ui/core';
import Chip from '@material-ui/core/Chip';

// import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

// Icons
import ClearIcon from '@material-ui/icons/Clear';  

import { ghosts } from "../../components/Ghosts/Ghosts.js"
// const [ emf_five, fingerprints, freezing, writing, spirit_box, ghost_orbs ] = ghostsigns

// Colors
// const good = "#33ff33"
// const bad = "#ff3333"
// const darkred = "#ee3333"

class GhostIndex extends React.Component {
  state = {
    tabindex: null
   }

   setTabIndex = (value) => {
    if (value < 0) return;
     this.setState({
       tabindex : value,
     })
   }

   findGhostIndex = (ghostname) => {
       var counter = 0
       ghosts.forEach((ghost)=>{
           if (ghostname === ghost.name) { 
               return counter;
           } else {
               counter++;
           }
       })
       return -1;
   }

  render () {
      const t = this.props.t;
      return (
        <div>
            <Toolbar>
                <Box display="flex" flexDirection="row" style={{width:'100%'}}>
                    <Box justifyContent="left" alignSelf="center" p={1} flexGrow={1}>
                        <div>
                            <Typography 
                                variant="h6" 
                                className={this.props.classes.grow} 
                                align="left" 
                                style = {{ 
                                fontFamily: "Schoolbell", 
                                }} >
                                {t("Ghostpedia")}
                            </Typography>
                        </div>
                    </Box>
                    <Box justifyContent="right" alignSelf="center" p={1}>
                        <IconButton 
                            onClick={() => this.props.todoClick(false)}
                            size='small'
                            >
                            <ClearIcon /> 
                        </IconButton>
                    </Box>
                </Box>
              </Toolbar>
              <Box display="flex" flexDirection="row" style={{width:'100%'}}>
                <Box p={1}>
                    <Tabs
                        orientation="vertical" 
                        variant="scrollable" 
                        indicatorColor="primary"
                        textColor="primary"
                        value={this.state.tabindex} 
                        onChange={(e,v)=>{this.setTabIndex(v)}} 
                        style={{width: 110, borderRight: `1px solid`, alignItems:"center"}}
                    >
                        {ghosts.map(ghost=>(
                            <Tab 
                                label={t(ghost.name)}
                            />
                        ))}
                    </Tabs>
                </Box>
                {ghosts.map(ghost=>(
                    <div hidden={this.state.tabindex !== ghosts.indexOf(ghost)}>
                        <Box p={3} style={{width:250, padding:"2px"}}>
                            <div>
                                <Typography 
                                    variant="body" 
                                    className={this.props.classes.grow} 
                                    align="left" 
                                    color="secondary"
                                    style = {{ 
                                    fontFamily: "Schoolbell", 
                                    }} >
                                    {t("Information about the...")}
                                </Typography>
                            </div>
                            <div>
                                <Typography 
                                    variant="h5" 
                                    className={this.props.classes.grow} 
                                    align="left" 
                                    style = {{ 
                                    fontFamily: "Schoolbell", 
                                    }} >
                                    {t(ghost.name)}
                                </Typography>
                            </div>
                            <div style={{marginTop:"4px"}}>
                                <Typography 
                                    variant="body" 
                                    className={this.props.classes.grow} 
                                    align="left" 
                                    color="secondary"
                                    style = {{ 
                                    fontFamily: "Schoolbell", 
                                    }} >
                                    {t("The signs...")}
                                </Typography>
                            </div>
                            <div>
                                {ghost.signs.map(sign => (
                                    <div>
                                        <Chip
                                        icon={sign.icon}
                                        label={t(sign.name)}
                                        size="small"
                                        />
                                    </div>
                                ))}
                            </div>
                            <div style={{marginTop:"4px"}}>
                                <Typography 
                                    variant="body" 
                                    className={this.props.classes.grow} 
                                    align="left" 
                                    color="secondary"
                                    style = {{ 
                                    fontFamily: "Schoolbell", 
                                    }} >
                                    {t("Strengths...")}
                                </Typography>
                            </div>
                            <div>
                                <List>
                                    {ghost.strengths.map(strength => (
                                    <ListItem style={{padding:"0px", maxWidth:210}}>
                                        <ListItemIcon style={{minWidth:'40px'}}>{strength.icon}</ListItemIcon>
                                        <ListItemText
                                            primaryTypographyProps = {{ 
                                            color: "secondary", 
                                            variant:"caption", 
                                            style:{ 
                                                fontFamily: "Schoolbell", 
                                                fontSize: 15, 
                                                // color: this.state.completed.includes(objective.text) ? "#" : "",
                                            }
                                            }} >
                                            {t(strength.description)}</ListItemText>
                                    </ListItem>
                                    ))}
                                </List>
                            </div>
                            <div style={{marginTop:"0px"}}>
                                <Typography 
                                    variant="body" 
                                    className={this.props.classes.grow} 
                                    align="left" 
                                    color="secondary"
                                    style = {{ 
                                    fontFamily: "Schoolbell", 
                                    }} >
                                    {t("Weaknesses...")}
                                </Typography>
                            </div>
                            <div>
                                <List>
                                    {ghost.weaknesses.map(weakness => (
                                    <ListItem style={{padding:"0px", maxWidth:210}}>
                                        <ListItemIcon style={{minWidth:'40px'}}>{weakness.icon}</ListItemIcon>
                                        <ListItemText
                                        primaryTypographyProps = {{ 
                                            color: "secondary", 
                                            variant:"caption", 
                                            style:{ 
                                              fontFamily: "Schoolbell", 
                                              fontSize: 15, 
                                              // color: this.state.completed.includes(objective.text) ? "#" : "",
                                            } 
                                          }}>{t(weakness.description)}</ListItemText>
                                    </ListItem>
                                    ))}
                                </List>
                            </div>
                        </Box>
                    </div>
                ))}
            </Box>    
    </div>
    )}
}
export default GhostIndex;