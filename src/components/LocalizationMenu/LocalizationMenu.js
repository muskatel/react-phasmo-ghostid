// Imports
import React from "react";

// Components
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';

class LocalizationMenu extends React.Component {
  state = {
   }

  render () {
    // let t = this.props.t;
      return (
        <div>
            <List dense>
                <ListItem>
                    <ListItemText>
                        <Typography variant="caption" display="block" gutterBottom>
                            Select a supported language:
                        </Typography>
                    </ListItemText>
                </ListItem>
                <Divider />
                <ListItem button onClick={()=>{this.props.onClickLang("en"); this.props.onClickClose();}} color="textPrimary">
                    <ListItemIcon><span role="img" aria-label="GB Flag">🇬🇧</span></ListItemIcon>
                    <ListItemText>{"English"}</ListItemText>
                </ListItem>
                <ListItem button onClick={()=>{this.props.onClickLang("af"); this.props.onClickClose();}} color="textPrimary">
                    <ListItemIcon><span role="img" aria-label="ZA Flag">🇿🇦</span></ListItemIcon>
                    <ListItemText>{"Afrikaans"}</ListItemText>
                </ListItem>
                <ListItem button onClick={()=>{this.props.onClickLang("es"); this.props.onClickClose();}} color="textPrimary">
                    <ListItemIcon><span role="img" aria-label="ES Flag">🇪🇸</span></ListItemIcon>
                    <ListItemText>{"Español"}</ListItemText>
                </ListItem>
                <ListItem button onClick={()=>{this.props.onClickLang("de"); this.props.onClickClose();}} color="textPrimary">
                    <ListItemIcon><span role="img" aria-label="DE Flag">🇩🇪</span></ListItemIcon>
                    <ListItemText>{"Deutsch"}</ListItemText>
                </ListItem>
                <Divider />
                <ListItem>
                    <ListItemText>
                        <Typography variant="caption" display="block" gutterBottom>
                            Don't see your language listed? Consider contributing to Ghost ID. See the about page. 
                        </Typography>
                    </ListItemText>
                </ListItem>
            </List>
        </div>
    )}
}
export default LocalizationMenu;