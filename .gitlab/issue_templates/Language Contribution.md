## Steps to contribute

1. Download the English `translation.json` file found here: https://gitlab.com/thelunararmy/react-phasmo-ghostid/-/raw/main/src/locales/en/translation.json (right click → save as...)
2. Open the file with a basic text editor like Notepad.
3. Edit the file and replace the second set of text on each line with the translated dialect. Do not replace the quotation marks.
2. On line 2 replace English with your language written in that language's dialect. I.e. Espanol not Spanish
3. On line 4 replace change `thelunararmy` to your username.
4. Upload your edited `translation.json` to this issue. There is a add file on the bottom right corner of this window.
5. Title this issue with the language in English
6. Complete the form below.

## Did you read the instructions above?

(Yes/No)

## Any comments about your translation?

(A particular word you coundnt translate or a localization issue I should be aware of.)

## Do you have a reddit profile?

(No/Link to it here i.e. `u/thelunararmy`)

/label ~localization
